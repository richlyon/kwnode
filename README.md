# kwNode

## Principles of operation

A measurement is a physical parameter obtained from a particular device at a specfic time.




Firmware for an environmental sensor.

```bash
~/.platformio/packages/toolchain-xtensa32/bin/xtensa-esp32-elf-addr2line \
  -pfiaC \
  -e .pio/build/concentrator/firmware.elf \
  0x40088b99:0x3ffbc0b0
```


# Project Notes

v 1.0.0 - basic LoRa / Websocket monitoring system
--------------------------------------------------
- TTGO ESP32 LoRa node 
    GREENHOUSE: temp, humidity, dewpoint, light
    OUTDOOR: temp, pressure
    battery operation
- TTGO ESP32 concentrator 
    WiFi/Websocket
    WiFi/MQTT
    OLED display
- NodeRed/Grafana
    Logging/Graphing

v 2.0.0 - ESP8266
-----------------
- ESP8266 Node
    LIVINGROOM: temp, humidity, CO2
    BEDROOM: temp, humidity, CO2

v 2.1.0 - Energy
----------------
- ESP32 Boiler node
- Raspberry Pi Electricity Node

v 2.2.0 - Visualisation
-----------------------
- ESP8288 Power visualiser
- ESP32 visualiser orb


TODO
========================================================================================

[ ] TTGO Concentrator: implement OLED display

[ ] Fix maximum period between LoRa sends
[ ] Design/fabricate TTGO LoRa boards (needs battery monitoring solution)
[ ] General: implement shared_ptr
[ ] General: refactor using FreeRTOS c++ wrapper
[ ] TTGO Node: implement sample frequency as platformio.ini config variable
[ ] TTGO Node: implement low power mode with platformio.ini config flag
[ ] Implement HLDC

[x] TTGO Concentrator: implement WiFi/Socket output
[x] General: implement vector
[x] Replace kwLED with an output device that reads commands from a 'flash command queue'
[x] TTGO Node: implement variable flashing (startup, measuring, transmitting)
[x] Build outdoor TTGO
[x] NodeRed: integrate new protobuf
[x] Implement RBE
[x] Tag LoRa signal quality with device name - NOT Lora name! lol
[x] Figure out battery monitoring 

Notes
====================================
OLED display tutorial: https://mansfield-devine.com/speculatrix/2019/01/ttgo-esp32-oled-display/

protobuf decode callback parameter: https://stackoverflow.com/questions/45844168/using-repeated-field-rule-in-a-message-with-nanopb-in-c/45844409#45844409

Feature: 
====================================
[1] Sample period is a node level configuration variable rather than sensor level. Remove from endpoint constructor, add to kwNode constructure and implement a reload timer.
[2] More expressive endpoint configuration. Maximum/minimum/rbe. Endpoint specific to allow specification of e.g. light intergration timee.


High Level Data Link Control (HDLC)
====================================
Use this to improve LoRa reliability and as a means of sending control info 
https://en.wikipedia.org/wiki/High-Level_Data_Link_Control#Control_field