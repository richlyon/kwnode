Import("env", "projenv")

env.Execute("$PYTHONEXE -m pip install --upgrade pip")
env.Execute("$PYTHONEXE -m pip install protobuf")

# Single action/command per 1 target
env.AddCustomTarget(
    "proto",
    None,
    ["python .pio/libdeps/concentrator/Nanopb/generator/nanopb_generator.py -I lib/kwNodeKit/src/proto -D lib/kwNodeKit/src/proto lib/kwNodeKit/src/proto/packet.proto",
    "python .pio/libdeps/concentrator/Nanopb/generator/nanopb_generator.py -I lib/kwNodeKit/src/proto -D lib/kwNodeKit/src/proto lib/kwNodeKit/src/proto/signal.proto"],
    title="Rebuild protobuf files"
)
