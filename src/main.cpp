/**
 * main.cpp: Kingswood Node firmware
 * Copyright (c) 2020 Richard J. Lyon
 *
 * See LICENSE for terms.
 */

#include <memory>
#include <string>

#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>

#include "kwSensorConfig.h"
#include "packet.pb.h"

#include <kwNode.h>
#include <kwSignal.h>

#if defined LOUNGE || BEDROOM
#include "secrets.h"
#include <kwDisplay.h>
#include <kwSCD30.h>
#include <kwWebsocket.h>
#endif

#if defined GREENHOUSE
#include <kwBattery.h>
#include <kwHDC1080.h>
#include <kwLora.h>
#include <kwVEML7700.h>
#endif

#if defined OUTDOOR
#include <kwBMP388.h>
#include <kwBattery.h>
#include <kwLora.h>
#endif

#if defined CONCENTRATOR
#include "secrets.h"
#include <kwLora.h>
#include <kwWebsocket.h>
#endif


#if GAS
#include "secrets.h"
#include <kwGas.h>
#include <kwWebsocket.h>
#endif

#define STRINGIFY( x ) #x
#define TOSTRING( x ) STRINGIFY( x )

const std::string cfg_FirmwareVersion = TOSTRING( VERSION );

void setup() {
    Serial.begin( 115200 );
    delay( 1000 );

    SignalPins signalPins = SignalPins_init_default;
    NodeMeta nodeMeta = NodeMeta_init_default;

#ifdef BEDROOM

    nodeMeta = { BoardType_esp32dev, NodeType_environment, NodeLocation_bedroom };
    kwNode node( nodeMeta, cfg_FirmwareVersion );

    signalPins = { ONBOARD_LED, ONBOARD_LED, ONBOARD_LED, ONBOARD_LED, ONBOARD_LED };
    kwSignal *signal = new kwSignal( signalPins );
    node.addOutput( signal );

    kwSCD30 *scd30 = new kwSCD30( snsr.co2, snsr.humidity, snsr.temperature );
    node.addInput( scd30 );

    kwDisplay *display = new kwDisplay();
    node.addOutput( display );

    kwWebsocket *websocket = new kwWebsocket( ssid, password, websockets_server_host, websockets_server_port, path );
    node.addOutput( websocket );

#endif

#ifdef LOUNGE

    nodeMeta = { BoardType_esp32dev, NodeType_environment, NodeLocation_lounge };
    kwNode node( nodeMeta, cfg_FirmwareVersion );

    signalPins = { GREEN_PIN, GREEN_PIN, GREEN_PIN, GREEN_PIN, GREEN_PIN };
    kwSignal *signal = new kwSignal( signalPins );
    node.addOutput( signal );

    kwSCD30 *scd30 = new kwSCD30( snsr.co2, snsr.humidity, snsr.temperature );
    node.addInput( scd30 );

    kwDisplay *display = new kwDisplay();
    node.addOutput( display );

    kwWebsocket *websocket = new kwWebsocket( ssid, password, websockets_server_host, websockets_server_port, path );
    node.addOutput( websocket );

#endif

#ifdef GREENHOUSE

    nodeMeta = { BoardType_ttgo_lora32_v1, NodeType_environment, NodeLocation_greenhouse };
    kwNode node( nodeMeta, cfg_FirmwareVersion );

    signalPins = { 2, 17, 23, 12, 13 };
    kwSignal *signal = new kwSignal( signalPins );
    node.addOutput( signal );

    kwHDC1080 *hdc1080 = new kwHDC1080( snsr.dewpoint, snsr.humidity, snsr.temperature );
    node.addInput( hdc1080 );

    kwVEML7700 *veml7700 = new kwVEML7700( snsr.lux );
    node.addInput( veml7700 );

    kwBattery *battery = new kwBattery( snsr.voltage );
    node.addInput( battery );

    kwLora *lora = new kwLora();
    node.addOutput( lora );

#endif

#ifdef OUTDOOR

    nodeMeta = { BoardType_ttgo_lora32_v1, NodeType_environment, NodeLocation_outdoor };
    kwNode node( nodeMeta, cfg_FirmwareVersion );

    signalPins = { 2, 17, 23, 12, 13 };
    kwSignal *signal = new kwSignal( signalPins );
    node.addOutput( signal );

    kwBMP388 *bmp388 = new kwBMP388( snsr.pressure, snsr.temperature );
    node.addInput( bmp388 );

    kwBattery *battery = new kwBattery( snsr.voltage );
    node.addInput( battery );

    kwLora *lora = new kwLora();
    node.addOutput( lora );

#endif

#ifdef CONCENTRATOR

    nodeMeta = { BoardType_ttgo_lora32_v2, NodeType_bridge, NodeLocation_mobile };
    kwNode node( nodeMeta, cfg_FirmwareVersion );

    signalPins = { 25, 2, 15, 13, 12 };
    kwSignal *signal = new kwSignal( signalPins );
    node.addOutput( signal );

    kwLora *lora = new kwLora();
    node.addInput( lora );

    kwWebsocket *websocket = new kwWebsocket( ssid, password, websockets_server_host, websockets_server_port, path );
    node.addOutput( websocket );

#endif

#ifdef GAS

    nodeMeta = { BoardType_esp32dev, NodeType_energy, NodeLocation_gas };
    kwNode node( nodeMeta, cfg_FirmwareVersion );

    signalPins = { BLUE_PIN, RED_PIN, RED_PIN, RED_PIN, RED_PIN };
    kwSignal *signal = new kwSignal( signalPins );
    node.addOutput( signal );

    kwWebsocket *websocket = new kwWebsocket( ssid, password, websockets_server_host, websockets_server_port, path );
    node.addOutput( websocket );

    kwGas *gas = new kwGas( snsr.gas );
    node.addInput( gas );

#endif

    node.describe();
    node.start();
}

void loop() {
}