#include <vector>

#include <Arduino.h>
#include <unity.h>

#include "packet.pb.h"
#include "utils.h"

void setUp() {
    Serial.begin( 115200 );
    delay( 1000 );
}

void tearDown() {
}

// Absolute to relative timetick conversion

void test_absolute_to_relative() {

    int32_t current_timestamp = 500;
    std::vector<Measurement> measurements;

    // Create three test measurements with timestamps [480, 490, 500]
    uint8_t n = 3;
    for ( int i = 0; i < n; i++ )
    {
        Measurement m;
        m.timestamp = current_timestamp - ( n - 1 - i ) * 10;
        measurements.push_back( m );
    }

    // Convert to relative -> [20, 10, 0]
    absoluteToRelative( measurements );

    TEST_ASSERT_EQUAL_INT32( 20, measurements[0].timestamp );
    TEST_ASSERT_EQUAL_INT32( 10, measurements[1].timestamp );
    TEST_ASSERT_EQUAL_INT32( 0, measurements[2].timestamp );
}

// Measurement retiming

void test_retime_measurements_all_positive() {

    // Test when all measurements were created after we were running
    // i.e. retimed timestamp are all positive

    int32_t our_current_timestamp = 800;
    int32_t their_current_timestamp = 500;

    std::vector<Measurement> measurements;
    std::vector<Measurement> retimed_measurements;

    // Create three test measurements with timestamps [480, 490, 500]
    uint8_t n = 3;
    for ( int i = 0; i < n; i++ )
    {
        Measurement m;
        m.timestamp = their_current_timestamp - ( n - 1 - i ) * 10;
        measurements.push_back( m );
    }

    TEST_ASSERT_EQUAL_INT32( 480, measurements[0].timestamp );
    TEST_ASSERT_EQUAL_INT32( 490, measurements[1].timestamp );
    TEST_ASSERT_EQUAL_INT32( 500, measurements[2].timestamp );

    // Retime them -> [780, 790, 800]
    retimeMeasurements( measurements, their_current_timestamp, our_current_timestamp );

    TEST_ASSERT_EQUAL_INT32( 780, measurements[0].timestamp );
    TEST_ASSERT_EQUAL_INT32( 790, measurements[1].timestamp );
    TEST_ASSERT_EQUAL_INT32( 800, measurements[2].timestamp );
}

void test_retime_measurements_some_negative() {

    // Test when a measurement was created before we were running
    // i.e. the retimed timestamp is negative

    int32_t our_current_timestamp = 10;
    int32_t their_current_timestamp = 80;

    std::vector<Measurement> measurements;
    std::vector<Measurement> retimed_measurements;

    // Create three test measurements with timestamps [80, 90, 100]
    uint8_t n = 3;
    for ( int i = 0; i < n; i++ )
    {
        Measurement m;
        m.timestamp = their_current_timestamp - ( n - 1 - i ) * 10;
        measurements.push_back( m );
    }

    TEST_ASSERT_EQUAL_INT32( 60, measurements[0].timestamp );
    TEST_ASSERT_EQUAL_INT32( 70, measurements[1].timestamp );
    TEST_ASSERT_EQUAL_INT32( 80, measurements[2].timestamp );

    // Retime them -> [0, 0, 10]
    retimeMeasurements( measurements, their_current_timestamp, our_current_timestamp );

    TEST_ASSERT_EQUAL_INT32( 0, measurements[0].timestamp );
    TEST_ASSERT_EQUAL_INT32( 0, measurements[1].timestamp );
    TEST_ASSERT_EQUAL_INT32( 10, measurements[2].timestamp );
}

void test_retime_measurements_with_delay() {

    // Test when transmitting measurements after a delay in receiving them
    // that retiming them includes the delay.

    int32_t our_current_timestamp = 800;   // This is the timestamp of the processor
    int32_t their_current_timestamp = 500; // This is the timestamp sent in the packet header

    std::vector<Measurement> measurements;
    std::vector<Measurement> retimed_measurements;

    // Create three delayed test measurements with timestamps [475, 485, 495]
    uint8_t n = 3;
    for ( int i = 0; i < n; i++ )
    {
        Measurement m;
        m.timestamp = their_current_timestamp - ( n - 1 - i ) * 10 - 5;
        measurements.push_back( m );
    }

    TEST_ASSERT_EQUAL_INT32( 475, measurements[0].timestamp );
    TEST_ASSERT_EQUAL_INT32( 485, measurements[1].timestamp );
    TEST_ASSERT_EQUAL_INT32( 495, measurements[2].timestamp );

    // Retime them -> [780, 790, 800]
    retimeMeasurements( measurements, their_current_timestamp, our_current_timestamp );

    TEST_ASSERT_EQUAL_INT32( 775, measurements[0].timestamp );
    TEST_ASSERT_EQUAL_INT32( 785, measurements[1].timestamp );
    TEST_ASSERT_EQUAL_INT32( 795, measurements[2].timestamp );
}

void setup() {
    delay( 2000 );
    UNITY_BEGIN();

    RUN_TEST( test_absolute_to_relative );
    RUN_TEST( test_retime_measurements_all_positive );
    RUN_TEST( test_retime_measurements_some_negative );
    RUN_TEST( test_retime_measurements_with_delay );

    UNITY_END();
}

void loop() {
}